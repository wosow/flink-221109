package com.atguigu.app.datastream;

import com.atguigu.bean.WaterSensor;
import org.apache.flink.api.common.eventtime.SerializableTimestampAssigner;
import org.apache.flink.api.common.eventtime.WatermarkStrategy;
import org.apache.flink.api.common.serialization.SimpleStringSchema;
import org.apache.flink.connector.kafka.source.KafkaSource;
import org.apache.flink.connector.kafka.source.enumerator.initializer.OffsetsInitializer;
import org.apache.flink.streaming.api.datastream.DataStreamSource;
import org.apache.flink.streaming.api.datastream.SingleOutputStreamOperator;
import org.apache.flink.streaming.api.datastream.WindowedStream;
import org.apache.flink.streaming.api.environment.StreamExecutionEnvironment;
import org.apache.flink.streaming.api.functions.windowing.ProcessWindowFunction;
import org.apache.flink.streaming.api.windowing.assigners.SlidingEventTimeWindows;
import org.apache.flink.streaming.api.windowing.time.Time;
import org.apache.flink.streaming.api.windowing.windows.TimeWindow;
import org.apache.flink.util.Collector;
import org.apache.flink.util.OutputTag;

import java.time.Duration;
import java.util.Iterator;

public class Flink03 {

    public static void main(String[] args) throws Exception {

        StreamExecutionEnvironment env = StreamExecutionEnvironment.getExecutionEnvironment();
        env.setParallelism(1);

        KafkaSource<String> kafkaSource = KafkaSource.<String>builder()
                .setBootstrapServers("hadoop102:9092")
                .setTopics("test_1109")
                .setGroupId("flink_03")
                .setStartingOffsets(OffsetsInitializer.latest())
                .setValueOnlyDeserializer(new SimpleStringSchema())
                .build();
        DataStreamSource<String> kafkaDS = env.fromSource(kafkaSource, WatermarkStrategy.<String>forBoundedOutOfOrderness(Duration.ofSeconds(2)).withTimestampAssigner(new SerializableTimestampAssigner<String>() {
            @Override
            public long extractTimestamp(String element, long recordTimestamp) {
                return Long.parseLong(element.split(",")[2]) * 1000L;
            }
        }), "kafka-source");

        kafkaDS.print("kafkaDS>>>>>>>>>>>");

        SingleOutputStreamOperator<WaterSensor> waterSensorDS = kafkaDS.map(line -> {
            String[] split = line.split(",");
            return new WaterSensor(split[0],
                    Double.parseDouble(split[1]),
                    Long.parseLong(split[2]));
        });

        OutputTag<WaterSensor> outputTag = new OutputTag<WaterSensor>("side") {
        };

        SingleOutputStreamOperator<WaterSensor> resultDS = waterSensorDS.keyBy(WaterSensor::getId)
                .window(SlidingEventTimeWindows.of(Time.seconds(30), Time.seconds(10)))
                .allowedLateness(Time.seconds(2))
                .sideOutputLateData(outputTag)
                .process(new ProcessWindowFunction<WaterSensor, WaterSensor, String, TimeWindow>() {
                    @Override
                    public void process(String key, ProcessWindowFunction<WaterSensor, WaterSensor, String, TimeWindow>.Context context, Iterable<WaterSensor> elements, Collector<WaterSensor> out) throws Exception {
                        System.out.println("WaterMark>>>>" + context.currentWatermark());
                        Iterator<WaterSensor> iterator = elements.iterator();

                        Double vc = 0.0D;

                        while (iterator.hasNext()) {
                            WaterSensor next = iterator.next();
                            vc += next.getVc();
                        }

                        out.collect(new WaterSensor(key, vc, 1L));
                    }
                });

        resultDS.getSideOutput(outputTag).print("side>>>>>");
        resultDS.print("result>>>>>");

        env.execute();
    }
}
