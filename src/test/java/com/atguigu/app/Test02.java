package com.atguigu.app;

import org.apache.flink.api.java.tuple.Tuple2;
import org.apache.flink.streaming.api.datastream.DataStream;
import org.apache.flink.streaming.api.environment.StreamExecutionEnvironment;
import org.apache.flink.table.api.Table;
import org.apache.flink.table.api.bridge.java.StreamTableEnvironment;
import org.apache.flink.types.Row;

/**
 * 2.写代码实现要求:
 * 	2.1 使用FlinkSQL读取Kafka数据(json:id,vc,ts),同时提取事件时间
 * 	2.2 计算每个id的最大vc
 * 	2.3 转换为流进行打印
 */
public class Test02 {

    public static void main(String[] args) throws Exception {

        //TODO 1.获取执行环境
        StreamExecutionEnvironment env = StreamExecutionEnvironment.getExecutionEnvironment();
        env.setParallelism(1);
        StreamTableEnvironment tableEnv = StreamTableEnvironment.create(env);

        //TODO 2.使用FlinkSQL读取Kafka数据(json:id,vc,ts),同时提取事件时间
        tableEnv.executeSql("" +
                "create table kafka_source(\n" +
                "    id string,\n" +
                "    vc double,\n" +
                "    ts bigint,\n" +
                "    rt AS TO_TIMESTAMP_LTZ(ts,0),\n" +
                "    WATERMARK FOR rt AS rt - INTERVAL '2' SECOND\n" +
                ") with (\n" +
                "    'connector' = 'kafka',\n" +
                "    'properties.bootstrap.servers' = 'hadoop102:9092',\n" +
                "    'properties.group.id' = 'test1',\n" +
                "    'scan.startup.mode' = 'group-offsets',\n" +
                "    'topic' = 'test',\n" +
                "    'format' = 'json'\n" +
                ")");

        //TODO 3.计算每个id的最大vc
        Table table = tableEnv.sqlQuery("select id,max(vc) max_vc from kafka_source group by id");

        //TODO 4.转换为流进行打印
        DataStream<Tuple2<Boolean, Row>> tuple2DataStream = tableEnv.toRetractStream(table, Row.class);
        tuple2DataStream.print(">>>");

        //TODO 5.启动
        env.execute();

    }
}
