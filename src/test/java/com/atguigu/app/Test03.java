package com.atguigu.app;

import org.apache.flink.streaming.api.environment.StreamExecutionEnvironment;
import org.apache.flink.table.api.Table;
import org.apache.flink.table.api.TableResult;
import org.apache.flink.table.api.bridge.java.StreamTableEnvironment;
import org.apache.flink.types.Row;

import static org.apache.flink.table.api.Expressions.$;

/**
 * 1.1 使用FlinkSQL读取Kafka数据(json:id,vc,ts)
 * 1.2 分别使用GroupWindow和TVF方式实现处理时间语义下10s滚动窗口计算每个传感器(id)的最高水位(vc)
 * 1.3 转换为流进行打印
 */
public class Test03 {

    public static void main(String[] args) throws Exception {

        //TODO 1.获取执行环境
        StreamExecutionEnvironment env = StreamExecutionEnvironment.getExecutionEnvironment();
        env.setParallelism(1);
        StreamTableEnvironment tableEnv = StreamTableEnvironment.create(env);

        //TODO 2.使用FlinkSQL方式读取Kafka数据,同时提取处理时间
        tableEnv.executeSql("" +
                "CREATE TABLE t1_pt(  " +
                "    id string,  " +
                "    ts bigint,  " +
                "    vc int, " +
                "    pt AS PROCTIME() " +
                ") WITH ( " +
                "  'connector' = 'kafka', " +
                "  'properties.bootstrap.servers' = 'hadoop102:9092', " +
                "  'properties.group.id' = 'test1', " +
                "  'scan.startup.mode' = 'group-offsets', " +
                "  'sink.partitioner' = 'fixed', " +
                "  'topic' = 'test_1109', " +
                "  'format' = 'json' " +
                ")");

        Table table = tableEnv.sqlQuery("select * from t1_pt");
        table.groupBy($("id"))
                .select($("id"), $("vc").sum());

        //select id,sum(vc) from t1_pt group by id;


        //TODO 3.  10秒滚动窗口  GroupWindow
        Table table1 = tableEnv.sqlQuery("" +
                "select " +
                "    tumble_start(pt,interval '10' second) stt, " +
                "    id, " +
                "    max(vc) " +
                "from t1_pt " +
                "group by id,tumble(pt,interval '10' second)");

        //TODO 3.  10秒滚动窗口  TVF
        Table table2 = tableEnv.sqlQuery("" +
                "select " +
                "    id, " +
                "    max(vc) " +
                "from table(tumble(table t1_pt,descriptor(pt),interval '10' second)) " +
                "group by id,window_start,window_end");

        //TODO 4.转换为流进行打印
        tableEnv.toAppendStream(table1, Row.class).print("t1>>>>>>>");
        tableEnv.toAppendStream(table2, Row.class).print("t2>>>>>>>");

        //TODO 5.启动任务
        env.execute();
    }
}