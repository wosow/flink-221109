package com.atguigu.app.flinksql.udf;

import com.atguigu.bean.WaterSensorAvg;
import org.apache.flink.table.functions.AggregateFunction;

public class AvgFunction extends AggregateFunction<Double, WaterSensorAvg> {

    //初始化
    @Override
    public WaterSensorAvg createAccumulator() {
        return new WaterSensorAvg();
    }

    //获取输出结果
    @Override
    public Double getValue(WaterSensorAvg waterSensorAvg) {
        if (waterSensorAvg.getCount() == 0L) {
            return null;
        } else {
            return waterSensorAvg.getVc() / waterSensorAvg.getCount();
        }
    }

    //每来一条数据计算一次
    public void accumulate(WaterSensorAvg acc, Double value) {
        acc.setVc(acc.getVc() + value);
        acc.setCount(acc.getCount() + 1L);
    }

    public void retract(WaterSensorAvg acc, Double value) {
        acc.setVc(acc.getVc() - value);
        acc.setCount(acc.getCount() - 1L);
    }

    public void resetAccumulator(WaterSensorAvg acc) {
        acc.setVc(0.0D);
        acc.setCount(0L);
    }
}
