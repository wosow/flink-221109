package com.atguigu.app.flinksql.udf;

import org.apache.flink.table.annotation.DataTypeHint;
import org.apache.flink.table.annotation.FunctionHint;
import org.apache.flink.table.functions.TableFunction;
import org.apache.flink.types.Row;

@FunctionHint(output = @DataTypeHint("ROW<word STRING, length INT>"))
public class SplitFunction extends TableFunction<Row> {

    public void eval(String str) {
        for (String s : str.split("_")) {
            // use collect(...) to emit a row
            collect(Row.of(s, s.length()));
        }
    }

}
