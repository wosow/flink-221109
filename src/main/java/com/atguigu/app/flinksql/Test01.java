package com.atguigu.app.flinksql;

import com.atguigu.bean.WaterSensor;
import org.apache.flink.streaming.api.datastream.DataStream;
import org.apache.flink.streaming.api.datastream.DataStreamSource;
import org.apache.flink.streaming.api.datastream.SingleOutputStreamOperator;
import org.apache.flink.streaming.api.environment.StreamExecutionEnvironment;
import org.apache.flink.table.api.Table;
import org.apache.flink.table.api.bridge.java.StreamTableEnvironment;
import org.apache.flink.types.Row;

public class Test01 {

    public static void main(String[] args) throws Exception {

        //TODO 1.获取执行环境
        StreamExecutionEnvironment env = StreamExecutionEnvironment.getExecutionEnvironment();
        env.setParallelism(1);
        StreamTableEnvironment tableEnv = StreamTableEnvironment.create(env);

        //TODO 2.从端口读取数据
        DataStreamSource<String> textStream = env.socketTextStream("hadoop102", 9999);
        SingleOutputStreamOperator<WaterSensor> waterSensorDS = textStream.map(line -> {
            String[] split = line.split(",");
            return new WaterSensor(split[0],
                    Double.parseDouble(split[1]),
                    Long.parseLong(split[2]));
        });

        //TODO 3.将流转换为动态表
        Table table = tableEnv.fromDataStream(waterSensorDS);
        tableEnv.createTemporaryView("t1", table);

        tableEnv.createTemporaryView("t2", waterSensorDS);

        //TODO 4.持续查询
        Table table1 = tableEnv.sqlQuery("select * from " + table);
        Table table2 = tableEnv.sqlQuery("select * from t2");

        //TODO 5.直接打印 该打印方式为阻塞模式
        //table1.execute().print();
        //table2.execute().print();

        //TODO 6.将动态表转换为流
        DataStream<Row> rowDataStream = tableEnv.toAppendStream(table1, Row.class);
        DataStream<WaterSensor> waterSensorDataStream = tableEnv.toAppendStream(table1, WaterSensor.class);

        //TODO 7.打印流
        rowDataStream.print("rowDataStream>>>>>");
        waterSensorDataStream.print("waterSensorDataStream>>>>");

        //TODO 8.启动任务
        env.execute();
    }
}
