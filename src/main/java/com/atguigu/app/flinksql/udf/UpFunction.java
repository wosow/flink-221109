package com.atguigu.app.flinksql.udf;

import org.apache.flink.table.annotation.DataTypeHint;
import org.apache.flink.table.annotation.InputGroup;
import org.apache.flink.table.functions.ScalarFunction;

public class UpFunction extends ScalarFunction {
    public String eval(String input) {
        return input.toUpperCase();
    }
}
