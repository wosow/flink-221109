package com.atguigu.app;

import com.atguigu.bean.WaterSensor;
import org.apache.flink.streaming.api.datastream.SingleOutputStreamOperator;
import org.apache.flink.streaming.api.environment.StreamExecutionEnvironment;
import org.apache.flink.table.api.Table;
import org.apache.flink.table.api.bridge.java.StreamTableEnvironment;

import static org.apache.flink.table.api.Expressions.$;

/**
 * 1.写代码实现要求:
 * 1.1 使用DataStream方式读取端口数据(id,vc,ts)
 * 1.2 转换为动态表并提取处理时间
 * 1.3 计算每个id的最大vc
 * 1.4 输出到Kafka
 */
public class Test01 {

    public static void main(String[] args) {

        //TODO 1.获取执行环境
        StreamExecutionEnvironment env = StreamExecutionEnvironment.getExecutionEnvironment();
        env.setParallelism(1);
        StreamTableEnvironment tableEnv = StreamTableEnvironment.create(env);

        //TODO 2.使用DataStream方式读取端口数据(id,vc,ts)
        SingleOutputStreamOperator<WaterSensor> waterSensorDS = env.socketTextStream("hadoop102", 9999)
                .map(line -> {
                    String[] split = line.split(",");
                    return new WaterSensor(split[0],
                            Double.parseDouble(split[1]),
                            Long.parseLong(split[2]));
                });

        //TODO 3.转换为动态表并提取处理时间
        Table table = tableEnv.fromDataStream(waterSensorDS, $("id"), $("vc"), $("ts"), $("pt").proctime());
        tableEnv.createTemporaryView("t1", table);

        //TODO 4.计算每个id的最大vc
        Table resultTable = tableEnv.sqlQuery("select id,max(vc) max_vc from t1 group by id");
        tableEnv.createTemporaryView("result_table", resultTable);

        //TODO 5.输出到Kafka
        tableEnv.executeSql("" +
                "create table kafka_sink( " +
                "    id string, " +
                "    max_vc double, " +
                "    primary key (id) NOT ENFORCED  " +
                ") with ( " +
                "    'connector' = 'upsert-kafka', " +
                "    'properties.bootstrap.servers' = 'hadoop102:9092', " +
                "    'topic' = 'test', " +
                "    'key.format' = 'json', " +
                "    'value.format' = 'json' " +
                ")");
        tableEnv.executeSql("insert into kafka_sink select * from result_table");

    }

}
