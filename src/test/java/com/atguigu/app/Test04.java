package com.atguigu.app;

import org.apache.flink.streaming.api.environment.StreamExecutionEnvironment;
import org.apache.flink.table.api.bridge.java.StreamTableEnvironment;

import java.time.Duration;

public class Test04 {

    public static void main(String[] args) {

        StreamExecutionEnvironment env = StreamExecutionEnvironment.getExecutionEnvironment();
        env.setParallelism(1);
        StreamTableEnvironment tableEnv = StreamTableEnvironment.create(env);

        //设置TTL
        tableEnv.getConfig().setIdleStateRetention(Duration.ofSeconds(10));
        tableEnv.getConfig();
        tableEnv.getConfig().getConfiguration().setString("table.exec.state.ttl", "10s");

    }

}
